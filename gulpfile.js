var gulp = require("gulp");
var ts = require("gulp-typescript");
var tsProject = ts.createProject("tsconfig.json");
var watch = require('gulp-watch');
const { spawn } = require('child_process');
var nodemon = require('gulp-nodemon');

gulp.task("default", function () {
    return tsProject.src()
        .pipe(tsProject())
        .js.pipe(gulp.dest("dist"));
});
gulp.task("scripts", function () {
    return tsProject.src()
        .pipe(tsProject())
        .js.pipe(gulp.dest("dist"));
});
gulp.task('run_server', ()=> {
    const run = spawn('node', [' dist\\server.js']);

    run.stdout.on('data', (data) => {
    console.log(`stdout: ${data}`);
    });

    run.stderr.on('data', (data) => {
    console.log(`stderr: ${data}`);
    });

    run.on('close', (code) => {
    console.log(`child process exited with code ${code}`);
    });
})
gulp.task('nodemon', ()=> {
    nodemon({
        script: 'dist/server.js'
      , ext: 'js html'
      , env: { 'NODE_ENV': 'development' }
      })
})

gulp.task('watch', ['default',], function() {
    gulp.watch( "src/**/*.ts", ['default',]);
});

gulp.task('backend', ['watch','nodemon'])
